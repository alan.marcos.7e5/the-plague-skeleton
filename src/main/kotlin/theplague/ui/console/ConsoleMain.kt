package theplague.ui.console

import theplague.interfaces.Position
import theplague.logic.World
import java.lang.Math.random
import java.util.*
import kotlin.random.Random

fun main() {
    val world = World()
    val scanner = Scanner(System.`in`)
    val worldUi = WorldUi(scanner, world)
    worldUi.play()
}

/*
fun main() {
    val position = Position(Random.nextInt(1..4), Random.nextInt(6))
    println(position)
}*/
