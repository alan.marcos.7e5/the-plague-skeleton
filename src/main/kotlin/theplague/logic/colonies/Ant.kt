package theplague.logic.colonies

import theplague.interfaces.Position
import theplague.logic.friends.FriendsAnimals
import theplague.logic.friends.Lazy
import theplague.logic.item.weapon.Broom
import theplague.logic.item.weapon.Sword
import theplague.logic.item.weapon.Weapon
import kotlin.math.max
import kotlin.math.min
import kotlin.random.Random


class Ant(private val reproductionTax: Double = 0.3) : Colony() {
    override var size: Int = 1

    override fun willReproduce(): Boolean {
        val id = Random.nextDouble(1.0)
        return id in 0.0..this.reproductionTax
    }

    override fun reproduce() {
        this.size ++
    }

    override fun attacked(weapon: Weapon) {
        when (weapon){
            is Broom -> size = 0
            is Sword -> size--
            else -> size -= 2
        }
    }

    override fun colonizedBy(plague: Colony): Colony {
        return when (plague){
            is Dragon -> plague
            else -> {
                this.size = min(3, size+plague.size) //siempre retornará el valor minimo entre a y b
                this
            }
        }
    }

    override fun expand(actualPosition: Position, maxPosition: Position): List<Colonization> {
        val colony = Ant()
        val position = obtainSidePosition(actualPosition,maxPosition)
        val colonization = Colonization(colony, position)
        size = 3 //vuelvo a cambiar el size para que sea 3
        return listOf(colonization)
    }

    private fun obtainSidePosition(from: Position, maxPosition: Position): Position {
        val minX = max(0, from.x - 1)
        val maxX = min(maxPosition.x, from.x + 1)
        val minY = max(0, from.y - 1)
        val maxY = min(maxPosition.y, from.y + 1)
        val positions = mutableListOf<Position>()
        for (x in minX..maxX) {
            for (y in minY..maxY) {
                if (x != from.x || y != from.y)
                    positions += Position(x, y)
            }
        }
        return positions.random()
    }

    override fun attackedByAnimal(friendAnimal: FriendsAnimals): Boolean {
        return if (friendAnimal is Lazy) {
            size = 0
            true
        }else false
    }

    override val icon: String = " \uD83D\uDC1C"

}