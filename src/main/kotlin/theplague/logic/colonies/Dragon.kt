package theplague.logic.colonies

import theplague.interfaces.Position
import theplague.logic.friends.Duck
import theplague.logic.friends.FriendsAnimals
import theplague.logic.item.weapon.Sword
import theplague.logic.item.weapon.Weapon
import kotlin.math.min

class Dragon(private var timeToReproduce: Int = 5): Colony() {
    override var size: Int = 1

    override fun willReproduce(): Boolean {
        timeToReproduce --
        return if (timeToReproduce == 0 && !needsToExpand()){
            timeToReproduce = 5
            true
        }else false
    }

    override fun reproduce() {
        this.size ++
    }

    override fun attacked(weapon: Weapon){
        if (weapon is Sword) size--
    }

    override fun colonizedBy(plague: Colony): Colony {
        return when (plague){
            is Ant -> this
            else -> {
                this.size = min(3, size+plague.size) //siempre retornará el valor minimo entre a y b
                this
            }
        }
    }

    override fun expand(actualPosition: Position, maxPosition: Position): List<Colonization> {
        val colony = Dragon()
        val position = Position((0 until maxPosition.x).random(), (0 until maxPosition.y).random())
        val colonization = Colonization(colony, position)
        return listOf(colonization)
    }

    override fun attackedByAnimal(friendAnimal: FriendsAnimals): Boolean{
        return if (friendAnimal is Duck) {
            size = 0
            true
        }else false
    }
    override val icon: String = " \uD83D\uDC09"
}