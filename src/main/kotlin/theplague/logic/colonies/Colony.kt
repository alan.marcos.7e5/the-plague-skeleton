package theplague.logic.colonies

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.friends.FriendsAnimals
import theplague.logic.item.weapon.Weapon

abstract sealed class Colony(): Iconizable {
    abstract var size: Int
    abstract fun willReproduce(): Boolean
    abstract fun reproduce()
    fun needsToExpand(): Boolean{
        return this.size > 3 //si es mayor a 3 necesita expandirse
    }
    abstract fun attacked(weapon: Weapon)
    abstract fun colonizedBy(plague: Colony): Colony
    abstract fun expand(actualPosition: Position, maxPosition: Position): List<Colonization>
    abstract fun attackedByAnimal(friendAnimal: FriendsAnimals): Boolean
}
