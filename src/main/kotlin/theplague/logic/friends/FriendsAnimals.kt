package theplague.logic.friends

import theplague.interfaces.Iconizable

abstract class FriendsAnimals(): Iconizable {
    var timesLeft: Int = 3
    fun discountTimes(): Boolean{
        timesLeft--
        return timesLeft > 0
    }
}