package theplague.logic.item.vehicle

import theplague.interfaces.Position
import theplague.logic.item.Item

abstract class Vehicle() : Item(5) {
    abstract fun canMove(from: Position, to: Position): Boolean
}