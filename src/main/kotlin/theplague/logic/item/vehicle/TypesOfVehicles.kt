package theplague.logic.item.vehicle

import theplague.interfaces.Position

class OnFoot(): Vehicle() {
    override val icon: String
        get() = "\uD83D\uDEB6"

    override fun use() = true

    override fun canMove(from: Position, to: Position): Boolean {
     val verifyY = from.y == to.y + 1 || from.y == to.y - 1 ||
                from.y == to.y || from.y == to.y

        return when (from.x) {
            to.x + 1 -> verifyY
            to.x - 1 -> verifyY
            to.x -> verifyY
            else -> false
        }
    }

}

class Bicycle(): Vehicle(){
    override val icon: String
        get() = "\uD83D\uDEB2"

    override fun canMove(from: Position, to: Position): Boolean {
        val firstVerify = to.x <= from.x + 4 && to.y <= from.y + 4
        return if (firstVerify) {
            to.x >= from.x - 4 && to.y >= from.y - 4
        } else false
    }

}

class Helicopter(): Vehicle(){
    override val icon: String
        get() = "\uD83D\uDE81"

    override fun canMove(from: Position, to: Position): Boolean {
        return true
    }
}