package theplague.logic.item.weapon


class Hand(): Weapon() {
    override val icon: String
        get() = "\uD83D\uDC46"

    override fun use() = true
}

class Sword(): Weapon(){
    override val icon: String
        get() = "\uD83D\uDDE1"


}

class Broom(): Weapon(){
    override val icon: String
        get() = "\uD83E\uDDF9"

}