package theplague.logic.item

import theplague.interfaces.Iconizable

abstract class Item(var timesLeft: Int): Iconizable {
    open fun use(): Boolean { //open puede implementarse o usarse esta implementación
        timesLeft--
        return timesLeft > 0
    }
}