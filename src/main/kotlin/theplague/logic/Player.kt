package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.item.vehicle.Bicycle
import theplague.logic.item.vehicle.OnFoot
import theplague.logic.item.vehicle.Vehicle
import theplague.logic.item.weapon.Hand
import theplague.logic.item.weapon.Sword
import theplague.logic.item.weapon.Weapon

class Player(override var currentPosition: Position) : IPlayer, Iconizable {
    fun useWeapon() {
        val canUse = currentWeapon.use()
        if(!canUse) weapon = null
    }
    fun useVehicle(){
        val canUse = currentVehicle.use()
        if(!canUse) vehicle = null
    }
    fun heal(){ //the friends animals can heal the player
        if (livesLeft < 15) livesLeft++
    }

    override var turns = 0
    override var livesLeft: Int = 15
    var weapon: Weapon? = null
    override val currentWeapon: Weapon get() = weapon ?: Hand()  //El arma por defecto es su mano, la puede usar la cantidad de veces que quiera
    var vehicle : Vehicle? = null
    override val currentVehicle get() = vehicle ?: OnFoot() //Por defecto se mueve a pie, la cantidad de veces que quiera
    override val icon: String = "\uD83D\uDEB6" //También tendrá su ícono, por extender también Iconizable
}