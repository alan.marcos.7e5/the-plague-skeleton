package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.logic.colonies.Colony
import theplague.logic.friends.FriendsAnimals
import theplague.logic.item.Item
import theplague.logic.item.weapon.Weapon

class Territory() : ITerritory {
    var player : Player? = null
    var item: Item? = null
    var colony: Colony? = null
    var friendAnimal: FriendsAnimals? = null

    fun exterminate(weapon: Weapon){
        if (colony != null){
            colony!!.attacked(weapon)
            if (colony!!.size <= 0) colony = null
        }
    }

    fun exterminateWithFriend(friendAnimal: FriendsAnimals): Boolean {
        var attackedResult = false
        if (colony != null){
            attackedResult = colony!!.attackedByAnimal(friendAnimal)
            if (colony!!.size <= 0) colony = null
        }
        return attackedResult
    }

    /**
     * Al hacer: val nameVariable = mutableList + List  -> suma los elementos de la List a la mutableList
     * y podremos retornar la mutableList que es del tipo iconizable, contendrá items, player y la colony
     * repetida colony.size veces
     */
    override fun iconList(): List<Iconizable> {
        val mutList = mutableListOf(player,item,friendAnimal).filterNotNull()+List(colony?.size ?: 0){colony}
        return mutList.filterNotNull()
    }
}