package theplague.logic

import theplague.interfaces.*
import theplague.logic.colonies.Ant
import theplague.logic.colonies.Colonization
import theplague.logic.colonies.Colony
import theplague.logic.colonies.Dragon
import theplague.logic.friends.Duck
import theplague.logic.friends.FriendsAnimals
import theplague.logic.friends.Lazy
import theplague.logic.item.Item
import theplague.logic.item.vehicle.Bicycle
import theplague.logic.item.vehicle.Helicopter
import theplague.logic.item.vehicle.Vehicle
import theplague.logic.item.weapon.Broom
import theplague.logic.item.weapon.Sword
import theplague.logic.item.weapon.Weapon


class World() : IWorld {
    override val height: Int = 8
    override val width: Int = 8
    override val player: Player = Player(Position(width/2, height/2))
    override val territories: List<List<Territory>>
         = List(this.height){ List(this.width){Territory()} }

    private val playerTerritory get() = territories[player.currentPosition.y][player.currentPosition.x]

    init{ //cuando se iniicializa la clase
        territories[height/2][width/2].player = player
    }

    override fun gameFinished() = player.livesLeft <= 0 //finishes game when lives=0

    override fun canMoveTo(position: Position): Boolean {
        return player.currentVehicle.canMove(player.currentPosition, position)
    }

    override fun exterminate() {
        if (playerTerritory.colony != null){ //TODO aunque el weapon no tenga efecto sobre una plaga, se descuenta su uso
            val weapon = player.currentWeapon
            playerTerritory.exterminate(weapon)
            player.useWeapon()
        }
    }

    override fun nextTurn() {
        generateNewItems()
        generateNewFriends()
        reproduce()
        generateNewColonies()
        expand()
        discountFriend()
    }

    private fun generateNewFriends() {
        val position = randomPosition()
        val friendAnimal = randomFriend()
        if (friendAnimal != null){
            placeFriend(friendAnimal, position)
        }
    }

    private fun placeFriend(friendAnimal: FriendsAnimals, position: Position) {
        val territory = territoryAt(position)
        if (territory.colony != null){
            if (territory.exterminateWithFriend(friendAnimal)){
                player.heal()
                territory.friendAnimal = friendAnimal
            }
        }else{
            territory.friendAnimal = friendAnimal
            player.heal()
        }
    }

    private fun discountFriend(){
        for (row in territories){
            for (territory in row){
                if (territory.friendAnimal != null)
                    if (!territory.friendAnimal!!.discountTimes()) territory.friendAnimal = null
            }
        }
    }

    private fun randomFriend(): FriendsAnimals? {
        val id = (1..10).random()
        return if (id <= 4) Lazy()
        else if (id >= 8) Duck()
        else null
    }

    override fun takeItem() {
        when (val item = playerTerritory.item){
            is Weapon -> player.weapon = item
            is Vehicle -> player.vehicle = item
        }
        playerTerritory.item = null
    }

    override fun moveTo(position: Position) {
        playerTerritory.player = null //take out icon of player in last territory
        player.currentPosition = position //change position of player
        player.turns += 1
        player.useVehicle()
        playerTerritory.player = player //put icon of player in territory
    }

    override fun takeableItem(): Iconizable? {
        return playerTerritory.item
    }

    //Métodos propios de World y ya no de su interface IWorld
    private fun randomPosition(): Position {
        return Position((0 until width).random(), (0 until height).random())
    }

    private fun territoryAt(position: Position) = territories[position.y][position.x]

    private fun generateNewColonies(){
        val position = randomPosition()
        val newColony = randomColony()
        if(newColony!=null) place(Colonization(newColony, position))
    }

    private fun randomColony(): Colony? {
        val id = (1..10).random()
        return if (id <= 3) Ant()
        else if (id == 4) Dragon()
        else null
    }

    private fun place(colonization: Colonization){
        val territory = territoryAt(colonization.position)
        if (territory.friendAnimal == null){ //where is an animal friend, won´t place any plague or expand
            if (territory.colony == null){
                territory.colony = colonization.colony
            }
            else {
                val mergedColony = territory.colony!!.colonizedBy(colonization.colony)
                territory.colony = mergedColony
            }
        }
    }
    private fun generateNewItems(){
        val position = randomPosition()
        val newItem = randomItem()
        territoryAt(position).item = newItem
    }

    private fun randomItem(): Item? {
        val id = (1..100).random()
        return if (id <= 25) Bicycle()
        else if (id in 26..50) Broom()
        else if (id in 50..60) Helicopter()
        else if (id in 60..70) Sword()
        else null
    }

    private fun reproduce(){
        for (row in territories) {
            for (territory in row) {
                if (territory.colony != null){
                    val colony = territory.colony!!
                    if (colony.willReproduce()) colony.reproduce()
                }
            }
        }
    }
    private fun expand(){
        for (y in territories.indices) {
            for (x in territories[y].indices) {
                if (territories[y][x].colony != null){
                    val territory = territories[y][x]
                    val colony = territory.colony!!
                    val maxPosition = Position(width-1, height-1)
                    if (colony.needsToExpand()){
                        player.livesLeft --
                        val expansion = colony.expand(Position(x,y),maxPosition)
                        for (colonization in expansion) place(colonization)
                    }
                }
            }
        }
    }
}