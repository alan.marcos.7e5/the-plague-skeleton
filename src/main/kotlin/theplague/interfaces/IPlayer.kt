package theplague.interfaces

import theplague.logic.item.vehicle.Vehicle

interface IPlayer {
    /**
     * Turns that the player has lived
     */
    var turns : Int

    var livesLeft: Int

    /**
     * The player current weapon
     */
    val currentWeapon : Iconizable

    /**
     * The player current vehicle
     */
    val currentVehicle : Vehicle
    /**
     * The player current position
     */
    val currentPosition : Position?
}